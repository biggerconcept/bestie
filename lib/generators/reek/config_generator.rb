# frozen_string_literal: true

require 'rails/generators/base'

module Reek
  class ConfigGenerator < Rails::Generators::Base
    source_root File.expand_path('templates', __dir__)

    def copy_default_config
      copy_file 'config.reek', 'config.reek'
    end
  end
end
