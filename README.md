# Bestie

Utility gem to enforce code quality and security best practice for rails applications and gems.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'bestie', aws: 'bestie'
```

And then execute:

```bash
bundle
```

## Usage

In applicaiton's `Rakefile`, add

```ruby
load 'tasks/bestie.rake'
```

Rake tasks are available after including the gem:

- [debride](https://github.com/seattlerb/debride)
- [fasterer](https://github.com/DamirSvrtan/fasterer)
- [flay](https://github.com/seattlerb/flay)
- [reek](https://github.com/troessner/reek)
- [rubocop](https://github.com/bbatsov/rubocop)
- [rubycritic](https://github.com/whitesmith/rubycritic)
- `bestie` => run all tasks to check ruby best practice

## Config Generators

If you wish to overload a quality checker's configuration, you can generate the base config using these handy rails generators:

### RuboCop

```bash
bundle exec rails g rubocop:config
```

### SimpleCov

```bash
bundle exec rails g simplecov:config
```

### Reek

```bash
bundle exec rails g reek:config
```

## License

This gem is not for redistribution.